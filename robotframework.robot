*** Settings ***
Documentation      Robot Framework test script
Library            SSHLibrary
Library            OperatingSystem

*** Variables ***
${target}            
${username}        user
${password}        user

*** Test Cases ***
testcase
    Create File     ${CURDIR}/example.txt    Hello World!
    Open Connection     ${target}        
    Login               ${username}    ${password}  
    Put File    ${CURDIR}/example.txt    /home/user
    ${cmd_out}    Execute Command    cat /home/user/example.txt
    Should Be Equal    ${cmd_out}    Hello World!
    Log To Console    ${cmd_out}    
    Close All Connections

